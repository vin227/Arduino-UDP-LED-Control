
void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    IPAddress remote = Udp.remoteIP();
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println(sizeof(packetBuffer) / sizeof(char));
    if(strcmp(packetBuffer, "is arduino yes") == 0) {
      Serial.println("replying:");
      Udp.beginPacket(Udp.remoteIP(), replyPort);
      Udp.write(ReplyBuffer);
      Udp.endPacket();
      Serial.print("replied: ");
      Serial.println(Udp.remoteIP());
    } else if(strcmp(packetBuffer, "lightsOn")){
      digitalWrite(ledPin, HIGH);
    } else if(strcmp(packetBuffer, "lightsOff")){
      digitalWrite(ledPin, LOW);
    } else if(ispunct(packetBuffer[3]) && ispunct(packetBuffer[7])) {
      colourInfo color = parseHexPacket(packetBuffer);
      displayColour(color);
    } else {
      Serial.print("idk wat this: ");
      Serial.println(sizeof(packetBuffer) / sizeof(char));
      Serial.println(packetBuffer);
    }
    memset(packetBuffer, 0, sizeof(packetBuffer));
  }
  delay(10);
}
