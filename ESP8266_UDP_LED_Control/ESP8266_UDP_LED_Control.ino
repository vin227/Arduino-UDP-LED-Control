
#include <SPI.h>         // needed for Arduino versions later than 0018
#include "WiFiEsp.h"
#include "WiFiEspUdp.h"

int ledPin = 4;
int redPin = 5;
int greenPin = 6;
int bluePin = 9;

#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial Serial2(2, 3); // RX, TX
#endif

char ssid[] = "OpenWrt";            // your network SSID (name)
char pass[] = "melkeetoimii";        // your network password
int status = WL_IDLE_STATUS;     // the Wifi radio's status
unsigned int localPort = 8888; 
unsigned int replyPort = 8899; 
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char  ReplyBuffer[] = "amled";       // a string to send back

IPAddress broadcastIP(255, 255, 255, 255);

// A UDP instance to let us send and receive packets over UDP
WiFiEspUDP Udp;


void setup() {
  pinMode(ledPin, OUTPUT);
  // initialize serial for debugging
  Serial.begin(9600);
  Serial.println("Setup");
  // initialize serial for ESP module
  Serial2.begin(9600);
  // initialize ESP module
  WiFi.init(&Serial2);
  
  // check for the presence of the shield
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue
    while (true);
  }
  // attempt to connect to WiFi network
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);
  }

  // you're connected now, so print out the data
  Serial.println("You're connected to the network");
  
  Udp.begin(localPort);
  //Alert everyone we are live.
  Udp.beginPacket(broadcastIP, 8899);
  Udp.write(ReplyBuffer);
  Udp.endPacket();

}
