
void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
//    Serial.print("Received packet of size ");
//    Serial.println(packetSize);
//    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
//    for (int i = 0; i < 4; i++) {
//      Serial.print(remote[i], DEC);
//      if (i < 3) {
//        Serial.print(".");
//      }
//    }
//    Serial.print(", port ");
//    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println(sizeof(packetBuffer) / sizeof(char));
    if(strcmp(packetBuffer, "is arduino yes") == 0) {
      Serial.println("replying:");
      Udp.beginPacket(Udp.remoteIP(), 8889);
      Udp.write(ReplyBuffer);
      Udp.endPacket();
      Serial.println("replied");
    } else if(ispunct(packetBuffer[3]) && ispunct(packetBuffer[7])) {
      colourInfo color = parseHexPacket(packetBuffer);
      displayColour(color);
    } else {
      Serial.print("idk wat this: ");
      Serial.println(sizeof(packetBuffer) / sizeof(char));
    }
    memset(packetBuffer, 0, sizeof(packetBuffer));
  }
  delay(10);
}
