
struct colourInfo //define data type for rgb value storage
{
  int r;
  int g;
  int b;
};

void displayColour (struct colourInfo x) {
  //for setting strip to static colour based on the rgb values input
  analogWrite(redPin, x.r);
  analogWrite(greenPin, x.g);
  analogWrite(bluePin, x.b);
}

struct colourInfo parseHexPacket (char packetBuffer[24]) {
  struct colourInfo result;
  int rgbIterator = 0;
  if(!ispunct(packetBuffer[3]) || !ispunct(packetBuffer[7])) {
    result.r = 0;
    result.g = 0;
    result.b = 0;
  } else {
    char * pch;
    pch = strtok(packetBuffer, ",");
    while (pch != NULL) {
      if(rgbIterator == 0) {
        result.r = atoi(pch);
      }
      if(rgbIterator == 1) {
        result.g = atoi(pch);
      }
      if(rgbIterator == 2) {
        result.b = atoi(pch);
      }
      Serial.println(atoi(pch));
      pch = strtok(NULL, ",");
      rgbIterator++;
    }
    return result;
  }
}

