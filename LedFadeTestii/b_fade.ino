void fade(String ledColour, int fromValue = 0, int toValue = 255, int fadeDelay = 5) { //fade specified color in or out with chosen delay
  int ledPin = 2;
  int ledBrightness = 0;

  //sets fade pin to corresponding colour pin
  if (ledColour == "red") {
    ledPin = redPin;
  } else if (ledColour == "green") {
    ledPin = greenPin;
  } else if (ledColour == "blue") {
    ledPin = bluePin;
  }

  if (fromValue < toValue) {
    //checks if we are fading in our out
    //write the first value
    analogWrite(ledPin, fromValue);
    while (fromValue < toValue) {
      //fade the color by changing
      fromValue++;
      analogWrite(ledPin, fromValue);
      delay(fadeDelay);
    }
  } else if (fromValue > toValue) {
    //checks if we are fading in our out
    //write the first value
    analogWrite(ledPin, fromValue);
    while (fromValue > toValue) {
      fromValue--;
      analogWrite(ledPin, fromValue);
      delay(fadeDelay);
    }
  }

}
