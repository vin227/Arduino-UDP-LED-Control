
void loop() {
  //color (white/pink) while switch is turned
  while (digitalRead(switch1) == 1) {
    displayColour(pink);
  }
  //reset the values
  analogWrite(redPin, 0);
  analogWrite(greenPin, 0);
  analogWrite(bluePin, 0);

  
  while (digitalRead(switch1) == 0) {    //while the switch is off show fade effect
  sensorValue = analogRead(sensorPin);
  fade("red", 0, 255, (sensorValue / 200) + 1);
  if (digitalRead(switch1) == 1) {
    break;
  }
  fade("green", 0, 255, (sensorValue / 200) + 1);
  if (digitalRead(switch1) == 1) {
    break;
  }
  fade("blue", 0, 255, (sensorValue / 200) + 1 );
  if (digitalRead(switch1) == 1) {
    break;
  }
  fade("green", 255, 0, (sensorValue / 200) + 1);
  if (digitalRead(switch1) == 1) {
    break;
  }
  fade("blue", 255, 0, (sensorValue / 200) + 1);
  if (digitalRead(switch1) == 1) {
    break;
  }
  fade("red", 255, 0, (sensorValue / 200) + 1);
  if (digitalRead(switch1) == 1) {
    break;
  }
  }
}
