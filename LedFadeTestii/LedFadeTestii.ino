neoint redBrightness = 0;
int greenBrightness = 0;
int blueBrightness = 0;
int sensorValue = 0;

int redPin = 3;
int greenPin = 5;
int bluePin = 6;
int sensorPin = A1;
int switch1 = 12;


struct colourInfo //define data type for rgb value storage
{
  int r;
  int g;
  int b;
}white, pink;

void setup() {
  // put your setup code here, to run once:
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  pinMode(switch1, INPUT);
  white.r = 255;
  white.g = 255;
  white.b = 255;
  pink.r = 255;
  pink.g = 0;
  pink.b = 150;

}


