




void displayColour (struct colourInfo x) {
  //for setting strip to static colour based on the rgb values input
  analogWrite(redPin, x.r);
  analogWrite(greenPin, x.g);
  analogWrite(bluePin, x.b);
}
